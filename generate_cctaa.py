# -*- coding: utf-8 -*-
"""
Generate the Chinese Cross-Topic Authorship Attribution corpus (CCTAA) from the LDC Chinese
Gigaword Second Edition (https://doi.org/10.35111/vr0r-sb06)

See the README for more information about this script.

"""

__author__ = "CCTAA Authors"
__version__ = "1.0.0"
__license__ = "ISC"

import os
import re
import gzip
import hashlib
import argparse
import pandas as pd
from tqdm import tqdm
from io import BytesIO

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def main(input, output):
    print(f"Generating CCTAA-v1.0.0 corpus...")
    xinhua_dir = os.path.join(input, "data", "xin_cmn")

    metadata = pd.read_csv("metadata.csv").assign(text="")
    yms = [gz_file.name[8:14] for gz_file in os.scandir(xinhua_dir)]

    for ym in tqdm(yms):
        gz_dir = [
            dir_.path
            for dir_ in os.scandir(xinhua_dir)
            if dir_.name.startswith("xin_cmn_" + ym)
        ][0]
        ids = metadata.loc[metadata.id.str.startswith("XIN_CMN_" + str(ym))].id.tolist()
        idxs = metadata.loc[metadata.id.str.startswith("XIN_CMN_" + str(ym))].index
        with gzip.open(gz_dir, "rb") as f:
            giga = f.read().decode("utf-8")
        root = ET.fromstring("<root>" + giga + "</root>")
        for idx in idxs:
            id_ = metadata.iloc[idx].id
            xp_ = f"./*[@id='{id_}']"
            text = ""
            for children in list(root.find(xp_)):
                text += "".join(children.itertext())
            metadata.loc[idx, "text"] = text[re.search("\(记者.*\)", text).end() :]

    _buffer = BytesIO()
    metadata.to_csv(_buffer, index=False)
    md5 = hashlib.md5(_buffer.getbuffer()).hexdigest()

    if md5 == "2e2a54811f59944968c6929b5ec891e7":
        print(f"MD5({md5}) passes check.")
        metadata.to_csv(os.path.join(output, "cctaa-v1.0.0.csv"), index=False)
        print(f"CCTAA-v1.0.0 corpus is generated successfully.")
    else:
        print(f"Failure. MD5({md5}) dose not match. Use intact Chinese Gigaword 2E.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate the Chinese Cross-Topic Authorship\
     Attribution corpus from the LDC Chinese Gigaword Second Edition."
    )
    parser.add_argument(
        "-i",
        "--input",
        default="../chinese-gigaword-2e",
        help="directory of the Chinese Gigaword Second Edition corpus",
    )
    parser.add_argument(
        "-o", "--output", default=".", help="directory of the generated CCTAA corpus"
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__),
    )
    args = parser.parse_args()

    main(args.input, args.output)
