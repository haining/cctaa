# -*- coding: utf-8 -*-
"""
Calculate a RoBERTa baseline for the Chinese Cross-Topic Authorship Attribution corpus (CCTAA).

Required packages to reproduce the RoBERTa baseline, in addition to requirements.txt:

torch==1.10.2
wandb==0.12.11
transformers==4.16.2

"""
import re
import torch
import wandb
import pandas as pd
from torch.utils.data import Dataset
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from transformers import Trainer, TrainingArguments
from transformers import AutoModelForSequenceClassification, AutoTokenizer, EarlyStoppingCallback


########################################################################################################################
# helper functions
class CCTAA_Dataset(Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {k: torch.tensor(v[idx]) for k, v in self.encodings.items()}
        item["labels"] = torch.tensor([self.labels[idx]])
        return item

    def __len__(self):
        return len(self.labels)


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    acc = accuracy_score(labels, preds)
    return {'accuracy': acc}


########################################################################################################################
# hparams
device = 'cuda:1'

# init wandb
run_name = 'cctaa-roberta-baseline'
wandb.init(project=run_name)

# read in cctaa
CCTAA_DIR = 'cctaa-v1.0.0.csv'
cctaa = pd.read_csv(CCTAA_DIR)
# remove spaces and direct quotations
train_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "train"].text]
val_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "validation"].text]
test_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "test"].text]
# read in labels
le = LabelEncoder()
train_label = le.fit_transform(cctaa.loc[cctaa.split == "train"].author.tolist())
val_label = le.transform(cctaa.loc[cctaa.split == "validation"].author.tolist())
test_label = le.transform(cctaa.loc[cctaa.split == "test"].author.tolist())

# vectorize data
tokenizer = AutoTokenizer.from_pretrained('uer/chinese_roberta_L-12_H-768')
train_encodings = tokenizer(train_text, truncation=True, padding=True, max_length=512)
val_encodings = tokenizer(val_text, truncation=True, padding=True, max_length=512)
test_encodings = tokenizer(test_text, truncation=True, padding=True, max_length=512)

# prepare dataset
train_dataset = CCTAA_Dataset(train_encodings, train_label)
val_dataset = CCTAA_Dataset(val_encodings, val_label)
test_dataset = CCTAA_Dataset(test_encodings, test_label)

# setup model
training_args = TrainingArguments(run_name=run_name,
                                  output_dir=run_name,
                                  seed=42,
                                  do_eval=True,
                                  learning_rate=3e-4,
                                  adam_beta1=0.99,
                                  adam_beta2=0.9999,
                                  adam_epsilon=1e-08,
                                  per_device_train_batch_size=6,
                                  per_device_eval_batch_size=6,
                                  warmup_ratio=.05,
                                  load_best_model_at_end=True,
                                  metric_for_best_model='eval_loss',
                                  greater_is_better=False,
                                  dataloader_num_workers=4,
                                  dataloader_pin_memory=True,
                                  logging_strategy='epoch',
                                  save_strategy='epoch',
                                  evaluation_strategy="epoch",
                                  fp16=False,
                                  overwrite_output_dir=True,
                                  num_train_epochs=1000,
                                  save_total_limit=10,
                                  report_to='wandb')

model = AutoModelForSequenceClassification.from_pretrained(
    pretrained_model_name_or_path="uer/chinese_roberta_L-12_H-768",
    num_labels=len(set(train_label))).to(device)

# train the model
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=val_dataset,
    compute_metrics=compute_metrics,
    callbacks=[EarlyStoppingCallback(early_stopping_patience=100)]
)
trainer.train()

# make prediction
prediction = trainer.predict(test_dataset)
wandb.log({"test_accuracy": prediction.metrics['test_accuracy']})

wandb.finish()
