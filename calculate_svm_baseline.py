# -*- coding: utf-8 -*-
"""
Calculate a linear SVM baseline learning with function word frequencies for the Chinese Cross-Topic Authorship
Attribution corpus (CCTAA).

See the README for more information about this script.

"""
import re
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from functionwords import FunctionWords
from sklearn.preprocessing import Normalizer, StandardScaler

# load data
try:
    cctaa = pd.read_csv(open("cctaa-v1.0.0.csv"))
except FileNotFoundError:
    print("CCTAA is not found. Run generate_cctaa.py first.")
    quit()

print("Calculating CCTAA SVM baseline may take several minutes.")
print("Loading and vectorizing data...")
# remove spaces and direct quotations
train_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "train"].text
]
val_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "validation"].text
]
test_text = [
    re.sub("\s|“[\u4E00-\u9FFF,。《》\(\);:‘’\!\?\s.]+?”", "", t)
    for t in cctaa.loc[cctaa.split == "test"].text
]

# vectorize text into function word frequencies
vectorizer = FunctionWords("chinese_simplified_modern")
X_train = np.array([vectorizer.transform(t) for t in train_text])
X_val = np.array([vectorizer.transform(t) for t in val_text])
X_test = np.array([vectorizer.transform(t) for t in test_text])
# read in labels
y_train = cctaa.loc[cctaa.split == "train"].author
y_val = cctaa.loc[cctaa.split == "validation"].author
y_test = cctaa.loc[cctaa.split == "test"].author
print("Setting pipeline and fitting model...")

# setup ml pipeline
pipe = Pipeline(
    [
        ("normalizer", Normalizer(norm="l1")),
        ("scaler", StandardScaler()),
        ("svm", SVC(C=1, kernel="linear", max_iter=-1, tol=1e-9)),
    ]
)

pipe.fit(X_train, y_train)

print(f"CCTAA SVM baseline accuracy of validation set: {pipe.score(X_val, y_val):.1%}")
print(f"CCTAA SVM baseline accuracy of test set: {pipe.score(X_test, y_test):.1%}")
